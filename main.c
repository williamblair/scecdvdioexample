#include <stdio.h>
#include <eekernel.h>
#include <eeregs.h>
#include <sifdev.h>
#include <sifrpc.h>
#include <libcdvd.h>

int main()
{
    char buf[256];
    int fd; /* test file descriptor */
    int fileSize; /* Total size of test file */

    /* CD/DVD Starting guide (sce/Docs&Training/CD-DVD Starting Guide pdf) */
    sceSifInitRpc(0);
    scePrintf("Image file: "IOP_IMAGE_FILE"\n");
    while (!sceSifRebootIop("cdrom0:\\"IOP_IMAGE_FILE";1")) {
        scePrintf("iop reset error\n");
    }
    while (!sceSifSyncIop());

    scePrintf("After sceSifSyncIop\n");
    sceSifInitRpc(0);
    sceSifLoadFileReset();
    sceFsReset();

    scePrintf("After sceFsReset\n");
    sceCdInit(SCECdINIT);
    sceCdMmode(SCECdCD); /* for DVD mode use SCECdDVD */

    scePrintf("After sceCdMode\n");

    /* Open file */
    fd = sceOpen("cdrom0:\\TEST.TXT;1", SCE_RDONLY);
    if (fd < 0) {
        scePrintf("Failed to open test.txt\n");
        goto endLoop;
    }

    /* Get the file size and read the file into buf */
    fileSize = sceLseek(fd, 0, SCE_SEEK_END);
    sceLseek(fd, 0, SCE_SEEK_SET);
    if (fileSize < 256) {
        sceRead(fd, (u_int*)buf, fileSize);
        buf[fileSize] = '\0';
        scePrintf("Read buffer: %s\n", buf);
    } else {
        scePrintf("File too large to read into buffer\n");
    }
    sceClose(fd);

endLoop:
    while (1);
    
    return 0;
}

